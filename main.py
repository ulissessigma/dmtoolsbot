import telepot
import random
import time
import configparser

# Configuring bot
config = configparser.ConfigParser()
config.read_file(open('config.ini'))

Bot = telepot.Bot(config['DEFAULT']['token'])

wakeBot = True
isclockLockOpen = True

ultimaHr = 0
ultimoMin = 0

def define_LastMsgTime():
	global ultimaHr
	global ultimoMin
	
	ultimaHr = time.gmtime()[3]
	ultimoMin = time.gmtime()[4]

def hasTimePassed(num):
	global ultimaHr
	global ultimoMin
	
	horaAtual = (time.gmtime()[3] * 60) + time.gmtime()[4]
	deltaTime = horaAtual - ((ultimaHr * 60) + ultimoMin)
	
	return deltaTime > num

def stay_Awake():
	global wakeBot
	global isclockLockOpen
	
	maxMinutosSemMensagem = 28
	
	if(hasTimePassed(maxMinutosSemMensagem)):
		
		if(isclockLockOpen):
			
			if(wakeBot):
				Bot.sendMessage('159774618', 'Estou acordado.')
				define_LastMsgTime()
				wakeBot = False
				
			else:
				wakeBot = True
		
			isclockLockOpen = False
		
	else:
		if(not isclockLockOpen):
			isclockLockOpen = True
			wakeBot = True
			

def test_command_parameters(array):
	return len(array) == 2

def process_rolldicemsg(msg):
	
	diceRolls = None
	
	if msg.startswith('/'):
		msg = msg.strip('/')
		msg = msg.lower()
		msg = msg.split('d')
		
		if(test_command_parameters(msg)):
			diceRolls='[ '
			
			for i in range( 0 , int(msg[0]) ):
				diceRolls = diceRolls + str( random.randint(1, int(msg[1])) )
				
				if( i != (int(msg[0])-1) ):
					diceRolls = diceRolls + ' , '
					
			diceRolls = diceRolls + ' ]'
		
	return diceRolls

def process_message(Msg):
	if(Msg != None):
		response = process_rolldicemsg(Msg['text']);

		if response != None:
			global wakeBot
			wakeBot = False
			define_LastMsgTime()
		
			Bot.sendMessage(Msg['chat']['id'], response)

Bot.message_loop(process_message)

while True:
	stay_Awake()
	pass
